package main

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"net/http"
	"regexp"
	"strconv"
)

type Parser struct {
	url string
}

func BuildParser(url string) Parser {
	return Parser{url}
}

func (parser Parser) getUrl() string {
	return parser.url
}

func (parser Parser) getPageUrl(pageNumber int64) string {
	return fmt.Sprintf("%s/%d", parser.getUrl(), pageNumber)
}

func (parser Parser) getPageAmount() int64 {
	response, _ := http.Get(parser.getUrl())

	if response.StatusCode == 200 {
		doc, _ := goquery.NewDocumentFromReader(response.Body)
		currentPageSpan := doc.Find("div.pagination_expanded span.current").First()

		if currentPageSpan.Length() > 0 {
			i, err := strconv.ParseInt(currentPageSpan.Text(), 10, 0)

			if err == nil {
				return i
			}
		}
	}

	return 0
}

func (parser Parser) extractPostId(selection *goquery.Selection) (uint64, bool) {
	divId, _ := selection.Attr("id")
	re := regexp.MustCompile(`\d+$`)

	if re.MatchString(divId) {
		postId, err := strconv.ParseUint(re.FindString(divId), 10, 0)

		if err == nil {
			return postId, true
		}
	}

	return 0, false
}

func (parser Parser) extractAuthorName(selection *goquery.Selection) (string, bool) {
	authorLink := selection.Find("div.uhead_nick a").First()

	if authorLink.Length() > 0 {
		return authorLink.Text(), true
	}

	return "", false
}

func (parser Parser) extractTags(selection *goquery.Selection) ([]string, bool) {
	tags := selection.Find("h2.taglist a")

	return tags.Map(func(i int, selection *goquery.Selection) string {
		return selection.Text()
	}), true
}

func (parser Parser) extract(selection *goquery.Selection) IPost {
	id, parsedPostId := parser.extractPostId(selection)
	authorName, parsedPostAuthorName := parser.extractAuthorName(selection)
	tags, _ := parser.extractTags(selection)

	if !parsedPostId || !parsedPostAuthorName {
		return Post{0, "", make([]string, 0), true}
	}

	return Post{id, authorName, tags, false}
}

func (parser Parser) parsePosts(pageNumber int64) []IPost {
	response, _ := http.Get(parser.getPageUrl(pageNumber))
	posts := make([]IPost, 0)

	if response.StatusCode == 200 {
		doc, _ := goquery.NewDocumentFromReader(response.Body)
		postsDivs := doc.Find("div.postContainer")

		postsDivs.Each(func(i int, selection *goquery.Selection) {
			post := parser.extract(selection)
			posts = append(posts, post)
		})
	}

	return posts
}
