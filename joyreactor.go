package main

type IPost interface {
	getId() uint64
	getAuthorName() string
	getTags() []string
	isNull() bool
}

type Post struct {
	id         uint64
	authorName string
	tags       []string
	null       bool
}

func (post Post) getId() uint64 {
	return post.id
}

func (post Post) getAuthorName() string {
	return post.authorName
}

func (post Post) getTags() []string {
	return post.tags
}

func (post Post) isNull() bool {
	return post.null
}
