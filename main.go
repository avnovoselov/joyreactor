package main

import (
	"fmt"
	"time"
)

const URL = "http://joyreactor.cc"

const BATCH = 5

func asyncParse(parser Parser, pageNumber int64, c chan []IPost) {
	fmt.Println(pageNumber)
	result := parser.parsePosts(pageNumber)
	fmt.Println(result)
	c <- result
}

func main() {
	parser := BuildParser(URL)
	amount := parser.getPageAmount()
	c := make(chan []IPost)

	for i := amount; i > 0; i -= 5 {
		fmt.Println("Batch", i)

		for j := int64(0); j < BATCH && i-j > 0; j++ {
			fmt.Println(" page", i-j)
			go asyncParse(parser, i-j, c)
		}
		for j := int64(0); j < BATCH && i-j > 0; j++ {
			fmt.Println(<-c)
		}
		time.Sleep(time.Second)
	}
}
