DROP TABLE IF EXISTS posts_tags;
DROP TABLE IF EXISTS posts;

CREATE TABLE posts
(
    id     INT GENERATED ALWAYS AS IDENTITY,
    author VARCHAR(255)   NOT NULL,
    rating DECIMAL(10, 5) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE posts_tags
(
    post_id INT,
    tag     VARCHAR(255) NOT NULL,
    PRIMARY KEY (post_id, tag),
    CONSTRAINT fk_post
        FOREIGN KEY (post_id)
            REFERENCES posts (id)
);